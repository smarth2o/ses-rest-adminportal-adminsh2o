#input district_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "SELECT d_avg.daily_avg, w_avg.weekly_avg, m_avg.monthly_avg " +
"FROM (SELECT d.oid, avg(d.avrg) daily_avg " +
"FROM (SELECT t.oid, t.smart_meter_id, AVG(t.consumption) avrg " +
"FROM (SELECT d.oid, sm.smart_meter_id, MAX(mr.total_consumption_adjusted) - MIN(mr.total_consumption_adjusted) AS consumption,  date(mr.reading_date_time) AS reading_date " +
        " FROM district d  " +
        " JOIN building b ON d.oid = b.district_oid  " +
        " JOIN smart_meter sm ON b.oid = sm.building_oid " + 
        " JOIN meter_reading mr ON sm.oid = mr.smart_meter_oid " + 
        " WHERE d.oid = " + district_id +
        " GROUP BY mr.smart_meter_oid, DATE(mr.reading_date_time)) t " +
"GROUP BY t.smart_meter_id) d) d_avg " +
"JOIN (SELECT d.oid, avg(d.avrg) weekly_avg " +
"FROM (SELECT t.oid, t.smart_meter_id, AVG(t.consumption) avrg " +
"FROM (SELECT d.oid, sm.smart_meter_id, MAX(mr.total_consumption_adjusted) - MIN(mr.total_consumption_adjusted) AS consumption,  date(mr.reading_date_time) AS reading_date " +
        " FROM district d  " +
        " JOIN building b ON d.oid = b.district_oid  " +
        " JOIN smart_meter sm ON b.oid = sm.building_oid " +
        " JOIN meter_reading mr ON sm.oid = mr.smart_meter_oid " +
        " WHERE d.oid = " + district_id +
        " GROUP BY mr.smart_meter_oid, WEEK(mr.reading_date_time)) t " +
"GROUP BY t.smart_meter_id) d) w_avg " +
"ON d_avg.oid = w_avg.oid " +
"JOIN (SELECT d.oid, avg(d.avrg) monthly_avg " +
"FROM (SELECT t.oid, t.smart_meter_id, AVG(t.consumption) avrg " +
"FROM (SELECT d.oid, sm.smart_meter_id, MAX(mr.total_consumption_adjusted) - MIN(mr.total_consumption_adjusted) AS consumption,  date(mr.reading_date_time) AS reading_date " +
        " FROM district d  " +
        " JOIN building b ON d.oid = b.district_oid  " +
        " JOIN smart_meter sm ON b.oid = sm.building_oid " +
        " JOIN meter_reading mr ON sm.oid = mr.smart_meter_oid " +
        " WHERE d.oid = " + district_id +
        " GROUP BY mr.smart_meter_oid, MONTH(mr.reading_date_time)) t " +
"GROUP BY t.smart_meter_id) d) m_avg " +
"ON d_avg.oid = m_avg.oid  " 


// println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("daily_avg", new Double(r[0]).doubleValue())
	    jGenerator.writeNumberField("weekly_avg", new Double(r[1]).doubleValue())
	    jGenerator.writeNumberField("monthly_avg", new Double(r[2]).doubleValue())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//
println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
//return ["json" : mapper]
return ["json" : json]