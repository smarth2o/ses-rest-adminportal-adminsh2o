#input Integer[] oid
#input Integer[] building_oid
#input Integer[] household_oid
#input String[] family_id
#input Integer[] secondo_smart_meter_oid

#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;


try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    // write to file
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()

    for (int i=0; i<oid.length; i++)
    {
        //write json
        jGenerator.writeStartObject()
          jGenerator.writeNumberField("oid", oid[i])
          jGenerator.writeNumberField("building_oid", building_oid[i]!=null?building_oid[i]:-1)
          
          // households
          jGenerator.writeFieldName("households")
          jGenerator.writeStartArray()
          for (int j=0; j<secondo_smart_meter_oid.length; j++)
          {
            // write household details
            if(oid[i] == secondo_smart_meter_oid[j])
            {
            	jGenerator.writeStartObject()
          		jGenerator.writeNumberField("oid", household_oid[j])
         		jGenerator.writeStringField("family_id", family_id[j])
         		jGenerator.writeEndObject()
         	}
          }
          jGenerator.writeEndArray()
         	
        jGenerator.writeEndObject()
     }

    jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()
   

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

return ["json" : json]
