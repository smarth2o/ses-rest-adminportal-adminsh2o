#input meter_id
#output json

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
	
Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

query = "select prim.qty + ifnull(adj.adj_qty, 0), prim.prim_date " +
		" from " +
		"(select max(total_consumption_adjusted) - min(total_consumption_adjusted) qty, date(max(m.reading_date_time)) prim_date " +
		"from meter_reading m  " +
		"left outer join smart_meter sm on sm.oid = m.smart_meter_oid    " +
		//"left outer join household h on sm.oid = h.smart_meter_oid    " +
		//"left outer join building b on b.oid = h.building_oid    " +
		//"left outer join neutral_user nu on nu.household_oid = h.oid    " +
        //"left outer join user u on u.oid = nu.user_oid    " +
		//"where u.oid = " + user_id + " and total_consumption_adjusted is not null   " +
		"where sm.oid = " + meter_id + " and total_consumption_adjusted is not null   " +
		"group by date(CONVERT_TZ(m.reading_date_time,'GMT','CET'))) prim left outer join  " +
		"(SELECT " +
    	"T1.min_cons - T2.max_cons adj_qty,  date(T2.max_time) adj_date  " +
		"FROM " +
    	"(select min(total_consumption_adjusted) min_cons, min(m.reading_date_time) min_time  " + 
		"from meter_reading m   " +
		"left outer join smart_meter sm on sm.oid = m.smart_meter_oid    " +
		//"left outer join household h on sm.oid = h.smart_meter_oid    " +
		//"left outer join building b on b.oid = h.building_oid    " +
		//"left outer join neutral_user nu on nu.household_oid = h.oid    " +
        //"left outer join user u on u.oid = nu.user_oid    " +
		//"where u.oid = " + user_id + "  and total_consumption_adjusted is not null   " +
		"where sm.oid = " + meter_id + "  and total_consumption_adjusted is not null   " +
		"group by date(CONVERT_TZ(m.reading_date_time,'GMT','CET')) " +
        "order by min_time desc) T1 INNER JOIN  " +
        "(select max(total_consumption_adjusted) max_cons, max(m.reading_date_time) max_time  " + 
		"from meter_reading m   " +
		"left outer join smart_meter sm on sm.oid = m.smart_meter_oid    " +
		//"left outer join household h on sm.oid = h.smart_meter_oid    " +
		//"left outer join building b on b.oid = h.building_oid    " +
		//"left outer join neutral_user nu on nu.household_oid = h.oid    " +
        //"left outer join user u on u.oid = nu.user_oid   " +
		//"where u.oid = " + user_id + "  and total_consumption_adjusted is not null  " +
		"where sm.oid = " + meter_id + "  and total_consumption_adjusted is not null   " +
		"group by date(CONVERT_TZ(m.reading_date_time,'GMT','CET')) " +
        "order by max_time desc) T2 on TIMEDIFF(T1.min_time, T2.max_time) = '01:00:00') adj       " +  
		"ON prim.prim_date = adj.adj_date  " +
		"ORDER BY prim.prim_date desc    "


//println query

def result=null
def jsonString = null

def ris=0
result = session.createSQLQuery(query).list()
def average = 0;

try {

    OutputStream out = new ByteArrayOutputStream()
    
    JsonFactory jfactory = new JsonFactory();

    /*** write to file ***/
    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
    jGenerator.writeStartArray()


    for (r in result){
    	//write json
    	jGenerator.writeStartObject()
	    jGenerator.writeNumberField("quantity", new Double(r[0]).doubleValue())
	 	jGenerator.writeStringField("timestamp", r[1].toString())
	    jGenerator.writeEndObject()

	}

	jGenerator.writeEndArray()
    jGenerator.close()

    json = out.toString()

} catch (JsonGenerationException e) {

    e.printStackTrace();

} catch (JsonMappingException e) {

    e.printStackTrace();

} catch (IOException e) {

    e.printStackTrace();

}

commit(session)

//
println json
ArrayNode mapper = new ObjectMapper().readTree(json);
//println mapper
//return ["json" : mapper]
return ["json" : json]